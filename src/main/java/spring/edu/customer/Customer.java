package spring.edu.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer {
	@Column(name="ID")
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	@Column(name = "NAME")
	private String name;
	@Column(name = "EMAIL")
    private String email;
	@Column(name = "ADDRESS")
    private String address;
    
    protected Customer() {
    }
 
    protected Customer(String name, String email, String address) {
        this.name = name;
        this.email = email;
        this.address = address;
    }
    
    public Long getId() {
		return id;
    }
    
	public void setId(Long id) {
		this.id = id;
    }
    
	public String getName() {
		return name;
    }
    
	public void setName(String name) {
		this.name = name;
    }
    
	public String getEmail() {
		return email;
    }
    
	public void setEmail(String email) {
		this.email = email;
    }
    
	public String getAddress() {
		return address;
    }
    
	public void setAddress(String address) {
		this.address = address;
	}
}
